package main

import (
	"fmt"
	"git.jacknet.io/S.D/Chip-8_Go/chip8"
)

func main() {
	prog := make([]byte, 6)
	cpu := chip8.NewCHIP8(prog)
	cpu.PerformCycle()
	fmt.Printf("This should print out zero: %d!\n", cpu.GetGraphicsBuffer()[0])
}
