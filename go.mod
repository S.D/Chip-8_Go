module git.jacknet.io/S.D/Chip-8_Go

go 1.15

require (
	github.com/llgcode/draw2d v0.0.0-20200930101115-bfaf5d914d1e
	github.com/markfarnan/go-canvas v0.0.0-20200722235510-6971ccd00770
)
